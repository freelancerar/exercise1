package ar.comviva.exerciseone.exceptions;


/**
 * @author lester
 * Thrown when a requested file does not exist
 */
public class FileDoesNotExistException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5722008285961903506L;

}
