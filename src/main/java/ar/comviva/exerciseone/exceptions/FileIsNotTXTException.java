/**
 * 
 */
package ar.comviva.exerciseone.exceptions;

/**
 * @author lester
 * Thrown when a requested file is not a text file
 */
public class FileIsNotTXTException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1715996544162034866L;

}
