package ar.comviva.exerciseone.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ar.comviva.exerciseone.dto.ResultsDTO;
import ar.comviva.exerciseone.exceptions.FileDoesNotExistException;
import ar.comviva.exerciseone.exceptions.FileIsNotTXTException;


/**
 * @author lester
 * Contains the logic for processing a txt file for specific porpouses 
 */
public class FileProcessor {
	
	
	/**
	 * Process the content of a text file for extracting all numbers within the file
	 * @param filePath The path to the target file
	 * @return A string with format N1 + N2 + Nn = SUM(N1 + N2 + Nn)
	 * @throws FileDoesNotExistException if file does not exist
	 * @throws FileIsNotTXTException if file is not a txt file
	 * @throws IOException unexpected error processing the file
	 */
	public static ResultsDTO extractAndProcessNumbersFromFile(String filePath) throws FileDoesNotExistException, FileIsNotTXTException, IOException {
		String returningString = "";
		
		//checkings
		File txtFile = new File(filePath);
		
		if(!txtFile.exists() || txtFile.isDirectory())
			throw new FileDoesNotExistException();
		
		/*if(isNotATXTFile(filePath))
			throw new FileIsNotTXTException();*/
		
		//read file content
		String content = new String(Files.readAllBytes(Paths.get(filePath, new String[] {}))) ;
		
		//process and extract numbers
		float sum = 0;
		
		Pattern p = Pattern.compile("\\d*\\.?\\d+");
        Matcher m = p.matcher(content);
       
        if(m.find()) {
        	//first number stuff
        	returningString = m.group();
        	
        	sum += Float.parseFloat(returningString);
        	
        	//resting numbers
	        while(m.find()) {
	        	String number = m.group();
	        	
	        	returningString += " + " + number;
	        	
	        	sum += Float.parseFloat(number);	        		        		        		           
	        }
	        
	        returningString += " = " + sum;
        }        
		
		return new ResultsDTO(returningString, sum);
	}			
}
