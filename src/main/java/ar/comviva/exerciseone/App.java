package ar.comviva.exerciseone;

import java.io.IOException;
import java.util.Scanner;

import ar.comviva.exerciseone.dto.ResultsDTO;
import ar.comviva.exerciseone.exceptions.FileDoesNotExistException;
import ar.comviva.exerciseone.exceptions.FileIsNotTXTException;
import ar.comviva.exerciseone.file.FileProcessor;

/**
 * @author lester
 * Console application
 */
public class App {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		boolean exit = false;		
		
		//messages
		String welcomeMSG = "Welcome to Problem #1";
		String filePathMSG = "Please enter the path of the file you want to process";
		String blankFilePathMSG = "File path cannot be empty.";
		String fileDoesNotExistMSG = "File does not exist";
		String fileIsNotTXTMSG = "The file is not a txt file";
		String unexpectedErrorMSG = "Sorry, there was an unexpected error while processing your file";
		String resultMSG = "The result of processing the file is shown below:";
		String exitMSG = "If you want to exit the application press 'Q', otherwise press any other key to continue";		
		String thanksMSG = "Thanks for using this application";
		
		System.out.println(welcomeMSG);
		
		//main loop
		do {									
			System.out.println(filePathMSG);
			
			boolean pathFileIsNotBlank = false;
			
			//filePath loop
			String filePath = "";
			do {
				filePath = keyboard.nextLine();
				
				if(filePath == null || filePath.equals(""))
					System.out.println(blankFilePathMSG);
				else
					pathFileIsNotBlank = true;
			}while(!pathFileIsNotBlank);
			
			//file processing and results showing
			try {
				ResultsDTO results = FileProcessor.extractAndProcessNumbersFromFile(filePath);
				
				String result = results.getSummaryExpression(); 
				
				System.out.println(resultMSG);
				System.out.println(result);
				
			} catch (FileDoesNotExistException e) {
				System.out.println(fileDoesNotExistMSG);
			} catch (FileIsNotTXTException e) {
				System.out.println(fileIsNotTXTMSG);
			} catch (IOException e) {
				System.out.println(unexpectedErrorMSG);
			}
			
			//exit verification
			System.out.println(exitMSG);
			
			char exitOption = keyboard.nextLine().charAt(0);
			
			if(exitOption == 'Q' || exitOption == 'q')
				exit = true;
			
		}while(!exit);

		//exit stuff
		System.out.println(thanksMSG);
		
		keyboard.close();
	}

}

