package ar.comviva.exerciseone.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import ar.comviva.exerciseone.exceptions.FileDoesNotExistException;
import ar.comviva.exerciseone.exceptions.FileIsNotTXTException;
import ar.comviva.exerciseone.file.FileProcessor;

public class TestCase {

	@Test
	public void testExtractAndProcessNumbersFromFile() {
		String output = "";
		
		try {
			System.out.println(new File(".").getCanonicalPath());
			
			output = FileProcessor.extractAndProcessNumbersFromFile("txtsamples\\sample1.txt").getSummaryExpression();			
		} catch (FileDoesNotExistException | FileIsNotTXTException e) {			
			
		} catch (IOException e) {
			
		}
		
		assertEquals("45 + 20 + 5 + 11 + 43.2 = 124.2", output);		
	}

}
