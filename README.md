# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Este es el repositorio GIT correspondiente al problema 1

El proyecto fue construido utilizando un proyecto Maven básico con una aplicación de Consola y el IDE Spring Tool Suite 4


### How do I get set up? ###

* Ejecutar la tarea de Maven "install" para que se genere el jar de la aplicación
* Ejecutar el jar generado en "target/exerciseone-0.0.1-SNAPSHOT-jar-with-dependencies.jar" 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Lester Hernandez (lestercujae@gmail.com)